# Calculator and BMI Calculator

  Web applications were built using  **Node.js** and **Express** framework, and  **Bootstrap** for design.

## Calculator

  In **_localhost:3000/_**  you will see two inputs and submit button, and Adding two numbers you will get total of two numbers.

## BMI Calculator

  In **_localhost:3000/bmicalculator_** you can check your BMI putting weight and height.

  Install and Run this project in your browser:
  ```
  git clone https://gitlab.com/Aselt/calapp.git
  git cd calapp
  npm install
  node calculator.js or nodemon calculator.js
  ```
  In your browser run **_localhost:3000_** and **_localhost:3000/bmicalculator_**.

  
