// jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
//Adding bodyParser to our code
app.use(bodyParser.urlencoded({extended: true}));

app.get("/", function(req, res){
  //console.log(__dirname);
  res.sendFile(__dirname + "/index.html");
});

// Adding post method to handle any post request that come to our home route.
app.post("/", function(req, res){
  //Get a data from callback function so we can calculate the output and then send the result back to the browser.
  // console.log(req.body.num1); //Comes from name attribute

  var num1 = Number(req.body.num1);
  var num2 = Number(req.body.num2);
  var result = num1 + num2;

  res.send("The result of the calculation is " + result);
});


//BMI Calculator GET and Post requests.
app.get("/bmiCalculator", function(req, res){
  res.sendFile(__dirname + "/bmiCalculator.html");
});

app.post("/bmiCalculator", function(req, res){
  var weight = parseFloat(req.body.weight);
  var height = parseFloat(req.body.height);
  var bmi = weight / (height * height);
  res.send("Your BMI is " + bmi);

});

app.listen(3000, function(){
  console.log("Server is running on port 3000");
});
